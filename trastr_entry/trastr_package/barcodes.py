def is_upc(string):
    stripped_string = string.strip()
    length = len(stripped_string)
    if stripped_string.isdigit() and (length == 8 or length == 12):
        return True
    else:
        return False
def is_ean(string):
    stripped_string = string.strip()
    length = len(stripped_string)
    if stripped_string.isdigit() and (length == 8 or length == 13):
        return True
    else:
        return False
