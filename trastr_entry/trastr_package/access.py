from flask import (
    current_app,
    _request_ctx_stack,
    has_request_context,
    render_template,
    make_response,
)
from functools import wraps
from flask_login.utils import _get_user
from werkzeug.local import LocalProxy

current_user = LocalProxy(lambda: _get_user())

no_role_permissions = [
    "View Product",
    "Add Product",
    "View Store",
    "View Price",
    "View User",
    "View Role",
]

basic_role_initial_permissions = [
    "View Product",
    "Add Product",
    "Edit Product",
    "View Store",
    "Add Store",
    "Edit Store",
    "View Price",
    "Add Price",
    "Edit Price",
    "View User",
    "Edit User",
    "View Role",
]
 
administrator_role_initial_permissions = [
    "View Product",
    "Add Product",
    "Edit Product",
    "Delete Product",
    "View Store",
    "Add Store",
    "Edit Store",
    "Delete Store",
    "View Price",
    "Add Price",
    "Edit Price",
    "Delete Price",
    "View User",
    "Add User",
    "Edit User",
    "Delete User",
    "View Role",
    "Add Role",
    "Edit Role",
    "Delete Role",
]

def permission_required(permission_name):
    """
    Grant access to route if user has role with permission with given
    permission name.
    """
    def inner_decorator(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            access_granted = False

            # check if current user has an account or is anonymous
            if hasattr(current_user, "access_roles"):
                for access_role in current_user.access_roles:
                    for permission in access_role.permissions:
                        if permission.name == permission_name:
                            access_granted = True
            else:
                # when user is anonymous, grant acccess based on no-role
                # permission list defined near beginning of access.py
                for no_role_permission in no_role_permissions:
                    if no_role_permission == permission_name:
                        access_granted = True
            if access_granted:
                return func(*args, **kwargs)
            else:
                return make_response(
                    render_template(
                        "error.html",
                        title="Access not permitted",
                        content="Account does not have a role with the correct permissions to access this page."
                    ),
                    403
                )
        return wrapped
    return inner_decorator


def role_required(role_name):
    """Grant access to route if user has role with given role name."""
    def inner_decorator(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            access_granted = False

            # check if current user has an account or is anonymous
            if hasattr(current_user, "access_roles"):
                for access_role in current_user.access_roles:
                    if access_role.name == role_name:
                        access_granted = True
            if access_granted:
                return func(*args, **kwargs)
            else:
                return make_response(
                    render_template(
                        "error.html",
                        title="Access not permitted",
                        content="Account does not have the correct role to access this page."
                    ),
                    403
                )
        return wrapped
    return inner_decorator
