from flask_wtf import FlaskForm
from flask_wtf.file import FileRequired
from wtforms import (
    StringField,
    TextAreaField,
    DecimalField,
    SelectField,
    IntegerField,
    SubmitField,
    PasswordField,
    BooleanField,
    FileField,
)
from wtforms.validators import (
    Length,
    NumberRange,
    DataRequired,
    Email,
    EqualTo,
    ValidationError,
)
from flask_login import current_user

from trastr_package.db import (
    db,
    bcrypt,
    User,
    UserVersion,
    Product,
    ProductVersion,
    get_user_by_username,
)
from trastr_package.openfoodfacts import api as offapi


class SignUpForm(FlaskForm):
    username = StringField(
        "Username", validators=[DataRequired(message="Please enter a username")]
    )
    email = StringField(
        "Email",
        validators=[
            DataRequired(),
            Email(message="Please enter a valid email address"),
        ],
    )
    password = PasswordField(
        "Password",
        validators=[
            DataRequired(),
            Length(
                min=10,
                max=200,
                message="Please enter a password between 10 and 200 characters in length",
            ),
        ],
    )
    confirm_password = PasswordField(
        "Confirm Password",
        validators=[
            DataRequired(),
            EqualTo("password"),
            Length(
                min=10,
                max=200,
                message="Please enter a password between 10 and 200 characters in length",
            ),
        ],
    )
    addr_housenumber = StringField("Housenumber", validators=[Length(max=80)])
    addr_housename = StringField("Housename", validators=[Length(max=80)])
    addr_conscriptionnumber = StringField(
        "Conscription Number", validators=[Length(max=80)]
    )
    addr_street = StringField("Street", validators=[Length(max=80)])
    addr_place = StringField("Place", validators=[Length(max=80)])
    addr_postcode = StringField("Postcode", validators=[Length(max=80)])
    addr_city = StringField("City", validators=[Length(max=80)])
    addr_country = StringField("Country", validators=[Length(max=80)])
    addr_hamlet = StringField("Hamlet", validators=[Length(max=80)])
    addr_suburb = StringField("Suburb", validators=[Length(max=80)])
    addr_subdistrict = StringField("Subdistrict", validators=[Length(max=80)])
    addr_province = StringField("Province", validators=[Length(max=80)])
    addr_state = StringField("State", validators=[Length(max=80)])
    addr_door = StringField("Door", validators=[Length(max=80)])
    addr_unit = StringField("Unit", validators=[Length(max=80)])
    addr_floor = StringField("Floor", validators=[Length(max=80)])
    addr_block = StringField("Block", validators=[Length(max=80)])

    magnitude_unit_system = SelectField(
        "Preferred Measurement System",
        coerce=int,
        choices=[],
    )
    submit = SubmitField("Sign Up")
    # Custom validation functions in this form must begin with 'validate_' or they don't get called!!!

    def validate_username(self, username):
        user_version = get_user_by_username(username.data)
        if user_version:
            raise ValidationError("Please choose another username.")


class EditUserForm(FlaskForm):
    username = StringField(
        "Username", validators=[DataRequired(message="Please enter a username")]
    )
    email = StringField(
        "Email",
        validators=[
            DataRequired(),
            Email(message="Please enter a valid email address"),
        ],
    )
    addr_housenumber = StringField("Housenumber", validators=[Length(max=80)])
    addr_housename = StringField("Housename", validators=[Length(max=80)])
    addr_conscriptionnumber = StringField(
        "Conscription Number", validators=[Length(max=80)]
    )
    addr_street = StringField("Street", validators=[Length(max=80)])
    addr_place = StringField("Place", validators=[Length(max=80)])
    addr_postcode = StringField("Postcode", validators=[Length(max=80)])
    addr_city = StringField("City", validators=[Length(max=80)])
    addr_country = StringField("Country", validators=[Length(max=80)])
    addr_hamlet = StringField("Hamlet", validators=[Length(max=80)])
    addr_suburb = StringField("Suburb", validators=[Length(max=80)])
    addr_subdistrict = StringField("Subdistrict", validators=[Length(max=80)])
    addr_province = StringField("Province", validators=[Length(max=80)])
    addr_state = StringField("State", validators=[Length(max=80)])
    addr_door = StringField("Door", validators=[Length(max=80)])
    addr_unit = StringField("Unit", validators=[Length(max=80)])
    addr_floor = StringField("Floor", validators=[Length(max=80)])
    addr_block = StringField("Block", validators=[Length(max=80)])

    magnitude_unit_system = SelectField(
        "Preferred Measurement System",
        coerce=int,
        choices=[],
    )
    submit = SubmitField("Save")
    # Custom validation functions in this form must begin with 'validate_' or they don't get called!!!

    def validate_username(self, username):
        user_version = get_user_by_username(username.data)
        if user_version and user_version.user_id != current_user.id:
            raise ValidationError("Please choose another username.")


class EditPasswordForm(FlaskForm):
    current_password = PasswordField(
        "Current Password",
        validators=[
            DataRequired(),
            Length(
                min=10,
                max=200,
                message="Please enter your password (between 10 and 200 characters in length)",
            ),
        ],
    )
    new_password = PasswordField(
        "New Password",
        validators=[
            DataRequired(),
            Length(
                min=10,
                max=200,
                message="Please enter a password between 10 and 200 characters in length",
            ),
        ],
    )
    confirm_new_password = PasswordField(
        "Confirm New Password",
        validators=[
            DataRequired(),
            EqualTo("new_password", message="Field must be equal to 'New Password'."),
            Length(
                min=10,
                max=200,
                message="Please enter a password between 10 and 200 characters in length",
            ),
        ],
    )
    submit = SubmitField("Change Password")
    
    def validate_current_password(self, current_password):
        """Ensure entered password is actually user's current password."""
        latest_user_version = current_user.user_versions[0]
        if not bcrypt.check_password_hash(
            latest_user_version.password,
            current_password.data
        ):
            raise ValidationError("Password is not correct")


class LogInForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember = BooleanField("Remember Me")
    submit = SubmitField("Log In")


class AddProductForm(FlaskForm):
    barcode = StringField("Barcode", validators=[DataRequired(), Length(max=80)])
    category = SelectField(
        "Category",
        coerce=int,
        choices=[],
    )
    magnitude = DecimalField("Magnitude")

    magnitude_unit = SelectField("Magnitude Unit", coerce=int, choices=[])
    submit = SubmitField("Add Product")


"""
    def validate_barcode(self, barcode):
        try:
            int(barcode.data)
        except ValueError:
            raise ValidationError("Barcodes must only contain numerals")
        except Exception as e:
            raise ValidationError(f"Something silly happened...try another code\n{e}")
        product = Products.query.filter_by(barcode=barcode.data).first()
        if product:
            raise ValidationError("This product is already in the database")
"""
class FindStoresForm(FlaskForm):
    latitude = DecimalField("Latitude", validators=[DataRequired()])
    longitude = DecimalField("Longitude", validators=[DataRequired()])
    radius = DecimalField("Radius")
    submit = SubmitField("Search")

class FindProductForm(FlaskForm):
    query = StringField("Query", validators=[DataRequired(), Length(max=100)])
    submit = SubmitField("Search")

class FindProductsForm(FlaskForm):
    name = StringField("Product Name", validators=[Length(max=100)])
    submit = SubmitField("Search")


class AddStoreForm(FlaskForm):
    brand = StringField("Brand", validators=[Length(max=80)])
    name = StringField("Specific Store Name", validators=[Length(max=80)])
    addr_country = SelectField(
        "Country",
        choices=[("USA", "United States")],
        validators=[DataRequired(), Length(max=80)],
    )
    addr_state = StringField("State", validators=[DataRequired(), Length(max=80)])
    addr_city = StringField("City", validators=[DataRequired(), Length(max=80)])
    addr_street = StringField("Street", validators=[DataRequired(), Length(max=80)])
    addr_housenumber = IntegerField("Building", validators=[DataRequired()])
    addr_unit = StringField("Unit", validators=[Length(max=80)])
    addr_postcode = StringField("Postal Code", validators=[Length(max=80)])
    comment = TextAreaField("Comment", validators=[Length(max=500)])
    submit = SubmitField("Add Store")

    def validate_brand_name(self, brand, name):
        if not (brand.data and name.data):
            raise ValidationError(
                "You must enter either a brand or a specific store name"
            )


class AddPriceForm(FlaskForm):
    product_id = IntegerField("Product ID", validators=[DataRequired()])
    price = DecimalField("Price", places=2, validators=[DataRequired()])
    currency = SelectField(
        "Currency",
        coerce=int,
    )
    store_id = IntegerField(
        "Store ID",
        validators=[DataRequired()],
    )
    submit = SubmitField("Add Price")


class AddRoleForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired(), Length(max=80)])
    
    # fields below have duplicate labels and descriptions due to the way the form logic reads input and assigns permissions to roles
    view_product = BooleanField("View Product", description="View Product")
    add_product = BooleanField("Add Product", description="Add Product")
    edit_product = BooleanField("Edit Product", description="Edit Product")
    delete_product = BooleanField("Delete Product", description="Delete Product")
    
    view_store = BooleanField("View Store", description="View Store")
    add_store = BooleanField("Add Store", description="Add Store")
    edit_store = BooleanField("Edit Store", description="Edit Store")
    delete_store = BooleanField("Delete Store", description="Delete Store")

    view_price = BooleanField("View Price", description="View Price")
    add_price = BooleanField("Add Price", description="Add Price")
    edit_price = BooleanField("Edit Price", description="Edit Price")
    delete_price = BooleanField("Delete Price", description="Delete Price")

    view_user = BooleanField("View User", description="View User")
    add_user = BooleanField("Add User", description="Add User")
    edit_user = BooleanField("Edit User", description="Edit User")
    delete_user = BooleanField("Delete User", description="Delete User")

    view_role = BooleanField("View Role", description="View Role")
    add_role = BooleanField("Add Role", description="Add Role")
    edit_role = BooleanField("Edit Role", description="Edit Role")
    delete_role = BooleanField("Delete Role", description="Delete Role")

    submit = SubmitField("Add")


class EditRoleForm(FlaskForm):
    name = StringField("Name", validators=[DataRequired(), Length(max=80)])
    
    # fields below have duplicate labels and descriptions due to the way the form logic reads input and assigns permissions to roles
    view_product = BooleanField("View Product", description="View Product")
    add_product = BooleanField("Add Product", description="Add Product")
    edit_product = BooleanField("Edit Product", description="Edit Product")
    delete_product = BooleanField("Delete Product", description="Delete Product")
    
    view_store = BooleanField("View Store", description="View Store")
    add_store = BooleanField("Add Store", description="Add Store")
    edit_store = BooleanField("Edit Store", description="Edit Store")
    delete_store = BooleanField("Delete Store", description="Delete Store")

    view_price = BooleanField("View Price", description="View Price")
    add_price = BooleanField("Add Price", description="Add Price")
    edit_price = BooleanField("Edit Price", description="Edit Price")
    delete_price = BooleanField("Delete Price", description="Delete Price")

    view_user = BooleanField("View User", description="View User")
    add_user = BooleanField("Add User", description="Add User")
    edit_user = BooleanField("Edit User", description="Edit User")
    delete_user = BooleanField("Delete User", description="Delete User")

    view_role = BooleanField("View Role", description="View Role")
    add_role = BooleanField("Add Role", description="Add Role")
    edit_role = BooleanField("Edit Role", description="Edit Role")
    delete_role = BooleanField("Delete Role", description="Delete Role")

    submit = SubmitField("Save")
"""
    def validate_barcode(self, barcode):
        if not db.session.query(Products).filter_by(barcode=barcode.data).first():
            raise ValidationError("This product does not exist")
"""


class UploadPricesForm(FlaskForm):
    csv = FileField("CSV File")
    submit = SubmitField("Upload Prices")


class ImportStoresForm(FlaskForm):
    # length set to accommodate longest placename in th world
    location = StringField("Location", validators=[DataRequired(), Length(max=120)])
    key_value_pairs = TextAreaField(
        "key=value pairs (each on a new line)",
        validators=[DataRequired(), Length(max=1000)],
    )
    submit = SubmitField("Import")
    # shop_department_store = BooleanField("shop=department_store")
    # shop_general = BooleanField("shop=general")
    # shop_kiosk = BooleanField("shop=kiosk")
    # shop_supermarket = BooleanField("shop=supermarket")
    # shop_wholesale = BooleanField("shop=wholesale")
    # shop_variety_store = BooleanField("shop=variety_store")
    # shop_chemist = BooleanField("shop=chemist")
    # shop_pharmacy = BooleanField("shop=pharmacy")
