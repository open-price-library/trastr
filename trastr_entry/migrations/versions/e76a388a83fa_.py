"""empty message

Revision ID: e76a388a83fa
Revises: ce318bf23212
Create Date: 2021-10-27 08:13:22.303189

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e76a388a83fa'
down_revision = 'ce318bf23212'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('store_version', sa.Column('user_id', sa.BigInteger(), nullable=False))
    op.create_foreign_key(None, 'store_version', 'user', ['user_id'], ['id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'store_version', type_='foreignkey')
    op.drop_column('store_version', 'user_id')
    # ### end Alembic commands ###
